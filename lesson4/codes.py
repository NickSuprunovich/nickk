source_dict = {'first_key': 'first_value', 'second_key': 'second_value'}
# ------ 1 zadanie ------
key = source_dict['first_key']
# ------ 2 zadanie ------
source_dict['first_key'] = '[1, 2, 4]'
# ------ 4 zadanie ------
s1 = source_dict.popitem()
s2 = source_dict.popitem()
source_dict[s1[1]] = s1[0]
source_dict[s2[1]] = s2[0]
source_dict[s1[1]] = s1[0]
# ------ или так ------
source_dict = {'first_key': 'first_value', 'second_key': 'second_value'}
s1 = list(source_dict.popitem())
s2 = list(source_dict.popitem())
s1.reverse(), s2.reverse()
source_dict = dict(((s1), (s2)))
# ------ 5 zadanie ------
spisok = list(source_dict.keys())
# ------ 6 zadanie ------
korteg = tuple(source_dict.values())
#------------------------
my_list = [[1, 'qwe'], [518, 'ttr'], [56, 'fty0']]
# ------ 7 zadanie ------
my_list.sort()
# ------ 8 zadanie ------
my_list.sort(key=lambda x: x[1], reverse=True)
# ------ 9 zadanie ------ 9 и 10 не одинаковы разве?
stroka = list('Hello World')
# ------ 10 zadanie ------ Если я правильно понял требование задачи, то так
stroka = 'Hello World'
mylist = stroka.split(' ')
# ------ 11 zadanie ------
source_dict = dict((('ff', 30), ('sd','mm')))
# ------ 14 zadanie ------
preobr = tuple(my_list)



