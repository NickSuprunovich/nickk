# 1
def f1(func, **kwargs):
    return func(kwargs)
def f2(**kwargs):
    pass
#2
class Test:
    def __init__(self, name, surname, *args, **kwargs):
        self.name = name
        self.surname = surname
#3
    def method1(self):
        return

    def method2(self):
        return
#4
    @staticmethod
    def method3(self):
        return

    @staticmethod
    def method4(self):
        return
#5 статический метод используется как атрибут класса
class Test:
    @classmethod
    def classmethod(cls, s):
        cls.da= s
    @staticmethod
    def statmethod():
        pass
#6
import json
with open('test.json', 'w+' ) as f:
    my_dict = json.load(f)
    my_dict["my_home"] = "44"
    s = my_dict.pop('c')
    my_dict['MAZ'] = s
    json_object = json.dumps(my_dict)
    f.write(json_object)

#7 конкатенация строк это их сложение пример: stroka = '{} {}'.format('Hello', 'World")
#8 with выпоняет какое нибудь действие например открытие файла with open('test.json', 'w+' ) as f:
#9 (1, 2, 3,) будет занимать места меньше тк это обрезанный по функционалу список
#10 Это преобразование одного типа в другой. list(), dict(), tuple()
#11 Наследование это возможность наследовать данные одного класса другому
class Test1():
        primer1 = 'hello'
class Test2(Test1):
        primer2 = 'world'
s1 = Test2()
print(s1.primer1)
#12
class Test():
    pass
class Test3(Test2, Test):
    pass
s2 = Test3()
print(s2.primer2)
#13 Test = type('Test', (), {'count': lambda self, x: x +1})
#14 функция map изменяет каждый элемент в списке
mapped_list = map(lambda x: x*2, [1, 2, 3, 4])
#15 Оператор in определяет есть ли a в b. Пример if a in b:
#16 Хранит обьекты dict
#17 в первом случаем передаём порядковый аргумент, во втором именованный
#18 функция super() возвращает обьект посредник классу super().__init__()
#19
with open('test.txt') as f:
    r = 0
    e = 0
    for i in f:
        r += 1
        s = f.readline()
        if f == 20:
            e += 1
            r = 0
        with open('test{}.txt'.format(e), 'w+') as w:
            w.write(s + '\n')

#20 скачивает все изменния с репозитория

#21
class MyPerse:
    def __init__(self):
        self.protocol = ''
    def get_protocol(self, url):
        if url.count('https')>= url.count('http'):
            self.protocol = 'https'
        else:
            self.protocol = 'https'
url_patterns = MyPerse('http://olol.ru/index.php?page=shop.product_details#info')


#22
class SimpleClass:
    def __init__(self, marka, color, toplivo, age):
        self.marka = marka
        self.color = color
        self.toplivo = toplivo
        self.age = age
    def get_marka(self, marka):
        self.marka = marka
    def get_color(self, color):
        self.color = color
    def get_toplivo(self, toplivo):
        self.toplivo = toplivo
    def get_age(self, age):
        self.age = age
    def get_param(self):
        return 'marka = {}, color = {}, toplivo = {}, age = {}'.format(self.marka, self.color, self.toplivo, self.age)
my_tel = SimpleClass('BMW', 'black', 'benzin', '1 god')
my_tel.get_marka('Lada')
my_tel.get_color('Green')
my_tel.get_toplivo('dizel')
my_tel.get_age('2 goda')
print(my_tel.get_param())