# ------ 2 zadanie ------
test = lambda x, y: x+y
print(test(test(1, 2), 1))
# ------ 3 zadanie ------
min([1, 2, 3, 67])
# ------ 5 zadanie ------
apr = [{'name': 'Tim', 'number': 1, 'is_man': True}]
gt = map(lambda x: x.keys(), apr)
print(list(gt))
# ------ 6 zadanie ------
# Это встроенные функции выполняющие преобразование типов. Например list(), tuple(), int(), dict().
# ------ 7 zadanie ------
# Конструкция нужна чтобы выводить результаты только в написанном(данном) модуле.Не импортировать лишнее.
# ------ 8 zadanie ------
# Круговой импорт это когда модули импортируют друг друга и зацикливаются.
# ------ 11 zadanie ------
x, y = [1, 2, 3], [4, 5, 6]
print(dict(zip(x, y)))
# ------ 12 zadanie ------
print(list(enumerate({'name': 'Tim', 'number': 1, 'is_man': True})))
# ------ 13 zadanie ------ range возвращает неизменяемую последовательность чисел
for i in range(10):
    print(i)