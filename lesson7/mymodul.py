def simple(x, y):
    if isinstance(x, int) and isinstance(y, int):
        return x+y
if __name__ == '__main__':
    print(simple(1, 11))
    print(simple(33, 11))