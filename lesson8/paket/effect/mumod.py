def simple(x, y):
    if isinstance(x, int) and isinstance(y, int):
        return x+y
if __name__ == '__main__':
    print(simple(1, 11))
    print(simple(33, 11))
    l = ['{}{}'.format(str(i),str(i-1)) for i in range(10)]
    with open('test.txt', 'a') as f:
        for index in l:
            f.write(index)