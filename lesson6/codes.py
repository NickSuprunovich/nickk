# ------ 1 i 2 zadanie ------
def simple(x, y):
    if isinstance(x, int) and isinstance(y, int):
        return x+y, x*y
    else:
        return ('Необходимо 2 числа')
print(simple(1, 11))
print(simple('df', 11))
# ------ 3 zadanie ------
def simple1(*args):
 return sum(args)
print(simple1(15, 5, 5))
# ------ 4 zadanie ------
def simple2(param = '', **kwargs):
    if param == 'keys':
        return kwargs.keys()
    elif param == 'values':
        return kwargs.values()
print(list(simple2(a=1, b=2, c=3, param='keys')))
# ------ 5 zadanie ------
def f1 (*args , **kwargs):
    if 'func' in kwargs and hasattr(kwargs['func'], '__call__'):
        kwargs['func'](*args)
def f2(*args):
    print(args)
f1(*(1, 2, 3), **{'func': f2})
# ------ 6 zadanie ------ правильно сделано или я не так понял?)
def f1 (*args , **kwargs):
    for k in kwargs.keys():
         if hasattr(kwargs[k], '__call__'):
             kwargs[k](*args)
def f2(*args):
    print(args)
def f3(*args):
    print(args)
f1(*(1, 2, 3, 4), **{'func': f2,'func2': f3, 'func3': f2})

