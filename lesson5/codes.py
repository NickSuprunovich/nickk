str1 = 'Top officials inside Spain’s ruling Popular Party, have appeared in court in Madrid on charges of fraud.Nearly 40 people, including businessman Francisco Correa, the alleged ring leader, are accused of paying bribes in exchange for contracts. In what has been billed as Spain’s trial of the year, the so called Gurtel case sees three former Popular Party treasurers among the defendants, including Luis Barcenas, a former ally of acting Prime Minister Mariano Rajoy. The corruption scandal, which has dogged Spain’s governing party since 2009, led to health minister Ana Mato resigning two years ago. Her husband is on trial for embezzlement and the family are accused of accepting gifts, including holidays and a Jaguar car. Meanwhile, as the Gurtel case got under way, another major political corruption trial was also playing out in Spain’s national court. Along with 64 other bankers, ex-IMF chief Rodrigo Rato, another leading figure in Prime Minister Rajoy’s party, is accused of misusing a corporate credit card for luxury purchases. Rato insists the expenses were legal and part of his salary. Rajoy and his party have distanced themselves from the former finance chief and also denied any wrong-doing in the Gurtel case, but both trials come at an awkward time for Spain’s premier as he seeks re-election.'
str1 = str1.lower()
for a in str1:
    if a == ',' or a == '.' or a == '!' or a == '?' or a == ';' or a == ':':
        str1 = str1.replace(a, '')
list1 = str1.split(' ')
list2 = list1.copy()
for a in list1:
    if list1.count(a) > 1:
        list1.remove(a)
dict_1 = dict.fromkeys(list1)
for a in list1:
    dict_1[a] = list2.count(a)
dict_1.update(all=len(list1))
print(dict_1)