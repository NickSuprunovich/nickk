str = 'https://test_url.org?repo=1&branch=first&user=test'
# ------ 1 zadanie ------
if str.find('https') > -1:
    print('Запрос выполнен по протоколу {}'.format('https'))
elif str.find('http') != -1:
    print('Запрос выполнен по протоколу {}'.format('http'))
# ------ 2 zadanie ------
s = str.index('?') + 1
print(str[s:])
# ------ 3 zadanie ------ Это наверно самый примитивный способ получения URL =)
s = str.index('/') + 2
s2 = str.index('?')
print(str[s:s2])
# ------ 4 zadanie ------ У меня всё сводится только к использованию index,
# поэтому интереса ради, слегка изменил строку запроса
str = 'https://test_url.org?repo=1&user=test&branch=first'
if str.find('user') != -1:
    s = str.rindex('user')
    s1 = str[s:len(str)]
    if s1.count('=') == 1:
        print(s1[s1.index('=')+1:len(s1)])
    else:
        print(s1[s1.index('=') + 1:s1.index('&')])

