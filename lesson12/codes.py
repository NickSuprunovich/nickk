import sys
class Data:
    data = []
    i = 0
    def __repr__(self):
        return '{}{}'.format(self.__class__.__name__, str(self.data))
class API:

    @classmethod
    def insert(cls, **kwargs):
        cls.data.append(dict(**kwargs))
        if len(cls.data) == 2 or sys.getsizeof(cls.data) > 100:
            cls.dump()


    @classmethod
    def select(cls, **kwargs):
        select_spisok = []
        for i in cls.data:
            if i == kwargs:
                select_spisok.append(i)
        for i in range(cls.i):
            with open('Baza{}.txt'.format(i), 'r') as f:
                spisok = eval(f.read())
                for a in spisok:
                    if a == kwargs:
                        select_spisok.append(a)
        return select_spisok

    @classmethod
    def delete(cls, **kwargs):
        for i in range(cls.i):
            with open('Baza{}.txt'.format(i), 'r') as f:
                del_spisok = []
                spisok = eval(f.read())
                for a in spisok:
                    if a != dict(kwargs):
                        del_spisok.append(a)
            with open('Baza{}.txt'.format(i), 'w+') as f:
                f.write(str(del_spisok))
        for i in cls.data:
            if i == dict(kwargs):
                cls.data.remove(i)

    @classmethod
    def update(cls, set, where):
        for i in range(cls.i):
            with open('Baza{}.txt'.format(i), 'r') as f:
                spisok = eval(f.read())
                for a in spisok:
                    if a == dict(where):
                        a.update(set)
            with open('Baza{}.txt'.format(i), 'w+') as f:
                f.write(str(spisok))
        for i in cls.data:
            if i == dict(where):
                i.update(set)
        return spisok

    @classmethod
    def all(cls):
        all = []
        for i in range(cls.i):
            with open('Baza{}.txt'.format(i), 'r') as f:
                spisok = eval(f.read())
                for a in spisok:
                       all.append(a)
        for y in cls.data:
            all.append(y)
        return all

    @classmethod
    def dump(cls):
        with open('Baza{}.txt'.format(cls.i), 'w+') as f:
            f.write(str(cls.data))
        cls.i += 1
        cls.data.clear()

    @classmethod
    def load(cls):
        all = []
        for i in range(cls.i):
            with open('Baza{}.txt'.format(i), 'r') as f:
                spisok = eval(f.read())
                for a in spisok:
                    all.append(a)
        return all

class First(Data, API):
    pass

f = First()
f.insert(name='Tom', color='Yellow')
f.insert(**{'name': 'Andry', 'color': 'black'})
f.insert(**{'name': 'Artur', 'color': 'black'})
f.insert(**{'name': 'Ivan', 'color': 'white'})
f.insert(**{'name': 'Ivan', 'color': 'white'})
f.update(set={'name': 'Anton', 'country': 'USA'}, where={'name':'Artur', 'color': 'black'})
f.delete(**{'name': 'Andry', 'color': 'black'})
print(f.select(**{'name': 'Ivan', 'color': 'white'}))
f.dump()
f.all()
print(f.load())
print(f.all())
print(f.data)